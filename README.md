# README #

A temporary fix for scheduling telegram messages.

* Write the message
* Pick a time for sending
* At the time app will open a window for sharing the message in telegram

More straight fixes require full dive into writing a custom telegram client.