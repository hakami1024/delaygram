package com.hakami1024

import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


inline fun <K, V> JSONArray.mapObjects(transform: (JSONObject) -> Pair<K, V>): Map<K, V> {
    val result = HashMap<K, V>()
    for (i in 0..length() - 1) {
        val p = transform(getJSONObject(i))
        result[p.first] = p.second
    }
    return result
}

inline fun <R> JSONArray.listObjects(transform: (JSONObject) -> R): List<R> {
    val result = ArrayList<R>(length())
    for (i in 0..length() - 1) {
        result.add(transform(getJSONObject(i)))
    }
    return result
}

fun Calendar.equalDay(c: Calendar): Boolean {
    return c.get(Calendar.DAY_OF_YEAR) == get(Calendar.DAY_OF_YEAR) && c.get(Calendar.YEAR) == get(Calendar.YEAR)
}

fun Calendar.copy(): Calendar =
        Calendar.getInstance().let {
            it.timeInMillis = timeInMillis
            it
        }

fun Calendar.isWeekend(): Boolean =
        when (get(Calendar.DAY_OF_WEEK)) {
            Calendar.SATURDAY -> true
            Calendar.SUNDAY -> true
            else -> false
        }

private val format = SimpleDateFormat("dd.MM.yyyy HH:mm")

fun Calendar.formattedDate(): String =
        format.format(time)

fun calendarFromString(str: String): Calendar =
        Calendar.getInstance().apply {
            setTime(format.parse(str))
        }


