package com.hakami1024

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find
import org.json.JSONObject
import java.util.*


fun createMessage(input: JSONObject) =
        createMessage(Calendar.getInstance().apply { setTimeInMillis(input.getLong("date")) },
                input.getString("text")
        ).apply {
//            intent = PendingIntent.readPendingIntentOrNullFromParcel(input.get("intent"));
        }

fun createMessage(date: Calendar, message: String) =
        Message(date, message)

open class Message(val date: Calendar, var text: String) {
    lateinit var intent: PendingIntent
    fun json() = JSONObject().apply {
        put("date", date.timeInMillis)
        put("text", text)
//        put("intent", intent.p)
    }
}

class MessageHolder(val view: ViewGroup, val date: TextView, val text: TextView) : RecyclerView.ViewHolder(view)

fun MessageHolder.bind(day: Message) {
    this.text.text = day.text
    this.date.text = day.date.formattedDate()
}

class MessagesListAdapter(val ctx: Context) : RecyclerView.Adapter<MessageHolder>() {
    private var items = arrayListOf<Message>()

    fun addMessage(msg: Message) {
        val alarmIntent = Intent(ctx, AlarmReceiver::class.java)
        alarmIntent.action = "android.intent.action.ALARM"
        alarmIntent.putExtra("message", msg.text)
        val alarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        msg.intent = PendingIntent.getBroadcast(ctx, 1, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.set(AlarmManager.RTC_WAKEUP, msg.date.timeInMillis,
                msg.intent)

        items.add(msg)

        notifyItemInserted(items.size - 1)
    }

    fun removeMessage(pos: Int) {
        items.get(pos).intent
        items.removeAt(pos)
        notifyItemRemoved(pos)
    }

    fun setMessages(days: List<Message>) {
        items.clear()
        items.addAll(days)
        notifyDataSetChanged()
    }

    fun days(): List<Message> = items.toList()

    override fun getItemCount(): Int =
            items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        val view = ctx.messageRow()
        val holder = MessageHolder(view, view.find(R.id.date), view.find(android.R.id.text1))

        return holder
    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) =
            holder.bind(items[position])
}
