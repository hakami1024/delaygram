package com.hakami1024

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Gravity
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView


val MARK_ROW_WIDTH = 160

fun Context.markView(): ViewGroup =
        linearLayout {
            layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
            gravity = Gravity.RIGHT
            textView {
                id = R.id.markName
                gravity = Gravity.RIGHT
                typeface = Typeface.defaultFromStyle(Typeface.ITALIC)
            }.lparams {
                width = dip(MARK_ROW_WIDTH)
            }

            textView {
                id = R.id.markValue
                typeface = Typeface.defaultFromStyle(Typeface.ITALIC)
            }
        }

fun Context.messageRow(): ViewGroup =
        verticalLayout() {
            layoutParams = RecyclerView.LayoutParams(matchParent, wrapContent).apply {
                bottomMargin = dip(5)
            }

            textView {
                id = R.id.date
                typeface = Typeface.defaultFromStyle(Typeface.BOLD)
            }.lparams {
                width = wrapContent
                height = matchParent
            }

            textView {
                id = android.R.id.text1
                hint = "No notes"
            }.lparams {
                width = matchParent
                height = wrapContent
                padding = dip(10)
            }

            verticalLayout {
                id = android.R.id.content
            }.lparams {
                width = matchParent
                height = wrapContent
            }
        }

fun Context.markTypeView(): ViewGroup =
        linearLayout {
            layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent).apply {
                horizontalPadding = dip(10)
            }

            editText {
                id = R.id.markName
                gravity = Gravity.CENTER_HORIZONTAL
                singleLine = true
            }.lparams {
                weight = 0.6F
            }

            textView {
                text = ": "
            }
            editText {
                id = R.id.markMin
                inputType = InputType.TYPE_CLASS_NUMBER
                gravity = Gravity.CENTER_HORIZONTAL
                singleLine = true
            }.lparams {
                weight = 0.2F
            }

            textView {
                text = " — "
            }

            editText {
                id = R.id.markMax
                inputType = InputType.TYPE_CLASS_NUMBER
                gravity = Gravity.CENTER_HORIZONTAL
                singleLine = true
            }.lparams {
                weight = 0.2F
            }
        }

fun Context.markFullView(): ViewGroup =
        linearLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent).apply {
                horizontalPadding = dip(15)
            }
            textView {
                id = R.id.markName
                gravity = Gravity.RIGHT
            }.lparams {
                width = dip(MARK_ROW_WIDTH)
            }
            textView {
                text = ": "
            }
            textView {
                id = R.id.markValue
                gravity = Gravity.CENTER_HORIZONTAL
            }.lparams {
                width = dip(26)
            }
            seekBar {
                id = R.id.markSeekBar
            }.lparams {
                weight = 1.0F
            }
        }

fun Context.settingsLayout(): LinearLayout =
        verticalLayout {
            recyclerView {
                id = android.R.id.list
                topPadding = dip(10)
                layoutManager = LinearLayoutManager(ctx)
            }.lparams {
                width = matchParent
                height = wrapContent
            }
        }

fun Context.todayLayout(): LinearLayout =
        verticalLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent).apply {
                horizontalPadding = dip(15)
            }

            recyclerView {
                id = android.R.id.list
                topPadding = dip(10)
                layoutManager = LinearLayoutManager(ctx)
            }.lparams {
                width = matchParent
                height = wrapContent
            }

            editText {
                id = android.R.id.text1
                singleLine = false
                imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
                hint = "Note about day"
            }.lparams {
                width = matchParent
            }
        }

fun Context.mainLayout(): LinearLayout {
    return verticalLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, matchParent).apply {
            padding = dip(10)
        }

        editText {
            id = R.id.message_text
            singleLine = false
            inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_CLASS_TEXT
            lines = 8
            isHorizontalScrollBarEnabled = false
            isVerticalScrollBarEnabled = true
        }

        linearLayout {
            editText {
                id = R.id.date_text
            }.lparams {
                width = wrapContent
                height = wrapContent
                weight = 1F
            }

            button {
                id = android.R.id.button1
                text = "Отправить"
                backgroundColor = resources.getColor(R.color.colorAccent)
                textColor = Color.WHITE
            }.lparams {
                width = wrapContent
                height = wrapContent
            }
        }

        recyclerView {
            id = android.R.id.list
            topPadding = dip(10)
            layoutManager = LinearLayoutManager(ctx)
        }.lparams {
            width = matchParent
            height = matchParent
        }
    }
}