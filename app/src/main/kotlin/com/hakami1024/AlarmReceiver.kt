package com.hakami1024

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            "android.intent.action.BOOT_COMPLETED" -> setAlarms()
            "android.intent.action.ALARM" -> {
                val appName = "org.telegram.messenger"
                //TODO: check
                val myIntent = Intent(Intent.ACTION_SEND)
                myIntent.setType("text/plain")
                myIntent.setPackage(appName)
                val stringExtra: String = intent.getStringExtra("message") ?: "NULL extra"
                myIntent.putExtra(Intent.EXTRA_TEXT, stringExtra)

                myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(Intent.createChooser(myIntent, "Share with"))
            }
        }
    }

    private fun setAlarms() {
        //TODO
    }
}