package com.hakami1024

import android.app.Activity
import android.content.Context
import org.json.JSONArray
import org.json.JSONObject
import java.io.File


fun Activity.loadDays(): List<Message> {
    val result = arrayListOf<Message>()

    val file = File(filesDir, "data")

    if (file.exists()) {
        val inStream = file.inputStream()
        if (inStream.available() > 0) {
            val byteArray = ByteArray(inStream.available())
            inStream.read(byteArray)

            val arr = JSONArray(String(byteArray))
            for (i in 0..arr.length() - 1) {
                val obj: JSONObject = arr[i] as JSONObject
                result.add(createMessage(obj))
            }
        }

        inStream.close()
    } else {
        file.createNewFile()
    }

    return result
}

fun Activity.saveDays(days: List<Message>) {

    val savingJSON = JSONArray()

    days.forEach { savingJSON.put(it.json()) }

    val outStream = openFileOutput("data", Context.MODE_PRIVATE)
    outStream.write(savingJSON.toString().toByteArray())
    outStream.flush()
    outStream.close()
}