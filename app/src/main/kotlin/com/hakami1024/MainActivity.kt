package com.hakami1024

import android.content.ComponentName
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.widget.Button
import android.widget.EditText
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import org.jetbrains.anko.contentView
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import java.util.*


class MainActivity() : AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    lateinit var adapter: MessagesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layout = mainLayout()

        setContentView(layout)

        supportActionBar?.title = "ValuedDiary"


        val dateView = layout.find<EditText>(R.id.date_text)
        dateView.onClick {
            val now = Calendar.getInstance()
            val dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH))
            dpd.show(fragmentManager, "Datepickerdialog")
        }

        val now = Calendar.getInstance()
        now.add(Calendar.HOUR, 1)
        dateView.setText(now.formattedDate())

        val receiver = ComponentName(this, AlarmReceiver::class.java)
        val pm = getPackageManager()

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)


        layout.find<Button>(android.R.id.button1).onClick {
            val inputView = layout.find<EditText>(R.id.message_text)
            if (inputView.text.isNotEmpty()) {
                val date = calendarFromString(dateView.text.toString())
                val text = inputView.text.toString()
                adapter.addMessage(Message(date,
                        text))
                inputView.setText("")
            }
        }

        val recyclerView = layout.find<RecyclerView>(android.R.id.list)
        adapter = MessagesListAdapter(this)
        recyclerView.adapter = adapter

        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback =
                object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean = true

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                        adapter.removeMessage(viewHolder.adapterPosition)
                    }
                }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        loadDays().forEach {
            adapter.addMessage(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menu?.add(R.string.settings)
//                ?.setOnMenuItemClickListener {
//                    startActivity<SettingsActivity>()
//                    true
//                }
//                ?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        val days = loadDays()
        adapter.setMessages(days)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        saveDays(adapter.days())

        super.onSaveInstanceState(outState)
    }


    override fun onDestroy() {
        saveDays(adapter.days())

        super.onDestroy()
    }


    override fun onTimeSet(dialog: TimePickerDialog, hourOfDay: Int, minute: Int, second: Int) {
        val year = dialog.arguments.getInt("year")
        val month = dialog.arguments.getInt("month")
        val day = dialog.arguments.getInt("day")

        val targetDate = Calendar.getInstance()
        targetDate.set(year, month, day, hourOfDay, minute)

        this.contentView
                ?.find<EditText>(R.id.date_text)
                ?.setText(targetDate.formattedDate())
    }

    override fun onDateSet(view: DatePickerDialog, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val now = Calendar.getInstance()
        val dpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true)

        if (dpd.arguments == null) {
            dpd.arguments = Bundle()
        }

        dpd.arguments.apply {
            putInt("year", year)
            putInt("month", monthOfYear)
            putInt("day", dayOfMonth)
        }

        dpd.show(fragmentManager, "Datepickerdialog")
    }

}
